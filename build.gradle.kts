plugins {
    java
    idea
    `java-library`
    `maven-publish`
    id("io.freefair.lombok") version "6.3.0"
    id("org.jetbrains.dokka") version "1.6.10"
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("org.openjfx.javafxplugin") version "0.0.10"
}

group = "com.runemate"
version = "1.15.6-SNAPSHOT"

repositories {
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenCentral()
    mavenLocal()
}

val bootClass by extra("com.runemate.client.boot.Boot")

val externalRepositoryUrl: String? by project
val externalRepositoryPrivateToken: String? by project

val runemate by configurations.creating {
    configurations["compileOnly"].extendsFrom(this)
    configurations["testCompileOnly"].extendsFrom(this)
}

dependencies {
    //Provided transitively at runtime
    runemate("com.runemate:runemate-client:3.11.1.0")
    runemate("com.google.code.gson:gson:2.8.9")
    runemate("org.jetbrains:annotations:22.0.0")
    runemate("com.google.guava:guava:31.0.1-jre")
    runemate("org.apache.commons:commons-lang3:3.12.0")
    runemate("org.apache.commons:commons-math3:3.6.1")
    runemate("org.apache.commons:commons-text:1.10.0")
    runemate("commons-io:commons-io:2.11.0")

    runemate(platform("org.apache.logging.log4j:log4j-bom:2.18.0"))
    runemate("org.apache.logging.log4j:log4j-api")
    runemate("org.apache.logging.log4j:log4j-core")

    implementation("org.json:json:20211205")
    implementation("org.jblas:jblas:1.2.5")
}

val javafxVersion = "18-ea+9"
val javafxModules = arrayOf(
        "javafx.base", "javafx.fxml", "javafx.controls", "javafx.media", "javafx.web", "javafx.graphics", "javafx.swing"
)

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

javafx {
    version = javafxVersion
    configuration = "runemate"
    modules(*javafxModules)
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
    withSourcesJar()
    withJavadocJar()
}

tasks.withType<Javadoc> {
    exclude("**/README.md")
    title = "RuneMate Game API $version"

    options {
        showFromPublic()
    }
}

task("launch", JavaExec::class) {
    group = "runemate"
    classpath = files(runemate, tasks.shadowJar)
    mainClass.set(bootClass)
}

task("testJar", Jar::class) {
    group = "build"
    from(sourceSets.test.get().output)
    archiveClassifier.set("test")
}

publishing {
    publications {
        register("mavenJava", MavenPublication::class) {
            from(components["java"])
        }
    }

    val repoUrl = externalRepositoryUrl
    val repoToken = externalRepositoryPrivateToken
    if (repoUrl != null && repoToken != null) {
        repositories.maven {
            name = "external"
            url = uri(repoUrl)
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = repoToken
            }
            authentication.create<HttpHeaderAuthentication>("header")
        }
    }
}
