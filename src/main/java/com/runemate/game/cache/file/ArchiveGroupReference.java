package com.runemate.game.cache.file;

import java.util.*;

public class ArchiveGroupReference {
    private HashMap<Integer, ArchiveFileReference> files;
    private int crc32;
    private int version;
    private byte[] whirlpool;
    private int fileCount;
    private int hashedName;

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int count) {
        if (files == null) {
            files = new HashMap<>(count, 1.0f);
        }
        this.fileCount = count;
    }

    public int getCRC32() {
        return crc32;
    }

    public void setCRC32(int crc32) {
        this.crc32 = crc32;
    }

    public byte[] getWhirlpool() {
        return whirlpool;
    }

    public void setWhirlpool(byte[] whirlpool) {
        this.whirlpool = whirlpool;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setFile(int identifier, ArchiveFileReference entry) {
        files.put(identifier, entry);
    }

    public ArchiveFileReference getFile(int identifier) {
        return files.get(identifier);
    }

    public int getHashedName() {
        return hashedName;
    }

    public void setHashedName(int hashedName) {
        this.hashedName = hashedName;
    }
}
