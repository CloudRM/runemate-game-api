package com.runemate.game.cache.file;

public class ArchiveFileReference {
    private int hashedName;

    public int getHashedName() {
        return hashedName;
    }

    public void setHashedName(int hashedName) {
        this.hashedName = hashedName;
    }
}
