package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.util.*;
import lombok.extern.log4j.*;

@Log4j2
public final class OSRSNpcContact {

    private final static int CONTAINER_ID = 75;

    private OSRSNpcContact() {
    }

    private static InterfaceComponent getContainer() {
        return Interfaces.newQuery().containers(CONTAINER_ID).grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER).widths(467).heights(250).results().first();
    }

    public static boolean isOpen() {
        final InterfaceComponent ic = getContainer();
        return ic != null && ic.isVisible();
    }

    private static boolean open() {
        log.info("Casting NPC Contact");
        return hasRunes() && Magic.Lunar.NPC_CONTACT.activate() && Execution.delayUntil(OSRSNpcContact::isOpen, 1200, 1800);
    }

    public static boolean cast(NpcContact.Contact contact) {
        log.info("Contacting {}", contact);
        if (!isOpen()) {
            if (isPreviousCast(contact)) {
                final InterfaceComponent component = Magic.Lunar.NPC_CONTACT.getComponent();
                return component != null && component.interact(contact.getAction());
            }
            if (!open()) {
                return false;
            }
        }
        return select(contact);
    }

    private static boolean isPreviousCast(NpcContact.Contact contact) {
        if (!ControlPanelTab.MAGIC.isOpen()) {
            ControlPanelTab.MAGIC.open();
        }
        final InterfaceComponent ic = Magic.Lunar.NPC_CONTACT.getComponent();
        if (ic != null) {
            for (final String string : ic.getActions()) {
                if (contact.getAction().matcher(string).matches()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean select(NpcContact.Contact contact) {
        final InterfaceComponent ic = contact.getComponent();
        final InterfaceComponent cont;
        return ic != null
            && (cont = getContainer()) != null
            && Interfaces.scrollTo(ic, cont)
            && ic.interact(contact.getAction())
            && Execution.delayWhile(OSRSNpcContact::isOpen, 1200, 1800);
    }

    private static boolean hasRunes() {
        final List<SpriteItem> provider = Inventory.newQuery()
            .names(Rune.AIR.getName(), Rune.ASTRAL.getName(), Rune.COSMIC.getName()).results()
            .asList();
        return Rune.AIR.getQuantity(provider) >= 2
            && Rune.ASTRAL.getQuantity(provider) >= 1
            && Rune.COSMIC.getQuantity(provider) >= 1;
    }

}
