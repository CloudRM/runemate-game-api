package com.runemate.game.api.osrs.entities;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.*;
import com.runemate.game.api.osrs.region.*;
import com.runemate.game.api.rs3.entities.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import javax.annotation.*;

public final class OSRSPlayer extends OSRSActor implements Player {
    private OpenPlayer player;
    private OpenPlayerComposite composite;

    public OSRSPlayer(long uid) {
        super(uid);
    }

    @Override
    public OpenPlayer actor() {
        if (player == null) {
            player = OpenPlayer.create(uid);
        }
        return player;
    }

    private OpenPlayerComposite composite() {
        if (composite == null || composite.getUid() != getComposite()) {
            composite = actor().createComposite();
        }
        return composite;
    }

    @RS3Only
    @Override
    public CombatGauge getAdrenalineGauge() {
        return null;
    }


    @Override
    public int getCombatLevel() {
        return actor().getCombatLevel();
    }

    @RS3Only
    @Override
    public SummonedFamiliar getFamiliar() {
        return null;
    }


    @Override
    public int getNpcTransformationId() {

        final OpenPlayerComposite composite = composite();
        if (composite.getUid() != 0) {
            return composite.getNpcId();
        }
        return -1;
    }

    @RS3Only
    @Nullable
    @Deprecated
    @Override
    public String getTitlePrefix() {
        return null;
    }

    @RS3Only
    @Nullable
    @Deprecated
    @Override
    public String getTitleSuffix() {
        return null;
    }


    @Override
    public int getTotalLevel() {
        return actor().getTotalLevel();
    }

    @RS3Only
    @Override
    public int getWildernessDepth() {
        return -1;
    }


    @Override
    public boolean isFemale() {
        final OpenPlayerComposite composite = composite();
        return composite.getUid() != 0 && composite.isFemale();
    }

    @Deprecated
    @Nonnull
    public List<ItemDefinition> getEquipment() {
        return getWornItems();
    }

    @Nonnull

    @Override
    public List<ItemDefinition> getWornItems() {
        final OpenPlayerComposite composite = composite();
        if (composite.getUid() != 0) {
            int[] clothing = composite.getClothing();
            //log.info("Equipment Size: " + clothing.length);
            List<ItemDefinition> equipment = new ArrayList<>(clothing.length);
            for (int clothingPiece : clothing) {
                if (clothingPiece != 0) {
                    //log.info("Appearance ID of " + clothingPiece + " for slot " + index);
                    if (clothingPiece >= 256 && clothingPiece < 512) {
                        //It's an IdentityKit instead of an Item
                        continue;
                    }
                    if (clothingPiece >= 512) {
                        ItemDefinition equipped = ItemDefinition.get(clothingPiece - 512);
                        if (equipped != null) {
                            equipment.add(equipped);
                        }
                    }
                }
            }
            return equipment;
        }
        return Collections.emptyList();
    }

    @Nullable
    @Override
    public ItemDefinition getWornItem(Equipment.Slot slot) {
        final OpenPlayerComposite composite = composite();
        if (composite.getUid() != 0) {
            int[] slotGarment = composite.getClothing();
            int clothingPiece = slotGarment[slot.getIndex()];
            if (clothingPiece != 0) {
                if (clothingPiece >= 256 && clothingPiece < 512) {
                    //It's an IdentityKit instead of an Item
                } else if (clothingPiece >= 512) {
                    return ItemDefinition.get(clothingPiece - 512);
                }
            }
        }
        return null;
    }

    @Override
    public int getTeamId() {
        return actor().getTeam();
    }

    @Override
    public Model getModel() {
        if (forcedModel != null) {
            return forcedModel;
        }
        var real = super.getModel();
        if (real instanceof RemoteModel) {
            return real;
        }
        if (cacheModel != null && cacheModel.isValid()) {
            return cacheModel;
        }
        int npcId = getNpcTransformationId();
        if (npcId != -1) {
            return OSRSNpcs.lookupModel(npcId, this);
        }
        long modelHash = getAppearanceHash();
        if (modelHash != -1) {
            long modelUid = actor().getModel(modelHash);
            if (modelUid != 0) {
                var points = OpenHull.lookupPoints(modelUid);
                if (points.length > 0) {
                    return cacheModel = new OSRSModel(modelUid, this, points);
                }
            }
        }
        return backupModel;
    }

    @Nullable
    @Override
    public Area.Rectangular getArea(Coordinate regionBase) {
        int npcTransformationId = getNpcTransformationId();
        if (npcTransformationId != -1) {
            int size = 1;
            NpcDefinition definition = NpcDefinition.get(npcTransformationId);
            if (definition != null) {
                NpcDefinition localState = definition.getLocalState();
                if (localState != null) {
                    definition = localState;
                }
                size = definition.getAreaEdgeLength();
            }
            Coordinate position = getPosition(regionBase);
            if (position == null) {
                return null;
            }
            if (size == 1) {
                return new Area.Rectangular(position);
            }
            return Area.rectangular(position, position.derive(size - 1, size - 1));
        }
        return super.getArea(regionBase);
    }


    @Override
    public String getName() {
        String name = OpenClient.getName(getNameComparable());
        if (!name.isEmpty()) {
            name = JagTags.remove(name);
            name = JagTags.nbspToSp(name);
            return name;
        }
        return null;
    }

    @Nonnull

    @Override
    public List<OverheadIcon> getOverheadIcons() {
        List<OverheadIcon> icons = new ArrayList<>(2);
        int iconIndex = actor().getSkullIconIndex();
        if (iconIndex != -1) {
            icons.add(new OverheadIcon(iconIndex, OverheadIcon.Type.SKULL));
        }
        iconIndex = actor().getPrayerIconIndex();
        if (iconIndex != -1) {
            icons.add(new OverheadIcon(iconIndex, OverheadIcon.Type.PRAYER));
        }
        return icons;
    }

    @Override
    public boolean interact(final String action) {
        return interact(action, getName());
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !Players.getLoaded(getName()).isEmpty();
    }


    private long getComposite() {
        return actor().getComposite();
    }


    private long getNameComparable() {
        return actor().getNameComparable();
    }

    @Override

    public long getAppearanceHash() {
        final OpenPlayerComposite composite = composite();
        if (composite.getUid() != 0) {
            return composite.getModelHash();
        }
        return -1;
    }
}
