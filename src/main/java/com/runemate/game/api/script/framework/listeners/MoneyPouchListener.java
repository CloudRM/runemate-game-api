package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

@RS3Only
@Deprecated
public interface MoneyPouchListener extends EventListener {
    void onContentsChanged(MoneyPouchEvent event);
}
