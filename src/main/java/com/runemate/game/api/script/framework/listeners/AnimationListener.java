package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

/**
 * @deprecated Use PlayerListener/NpcListener instead
 */
@Deprecated
public interface AnimationListener extends EventListener {
    void onAnimationChanged(AnimationEvent event);
}
