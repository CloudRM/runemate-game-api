package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.utilities;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import javax.annotation.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

public class PrayerAltarVertex extends UtilityVertex implements SerializableVertex {
    private Pattern name;
    private Pattern action;

    public PrayerAltarVertex(
        final Pattern name, final Pattern action, final Coordinate position,
        final Collection<WebRequirement>
            requirements
    ) {
        super(position, requirements);
        this.name = name;
        this.action = action;
    }

    public PrayerAltarVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    public GameObject getAltar() {
        return GameObjects.newQuery().names(getName()).actions(getAction()).on(getPosition())
            .results().first();
    }

    public Pattern getName() {
        return name;
    }

    public Pattern getAction() {
        return action;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getPosition())
            .append(getName().pattern())
            .append(getAction().pattern()).toHashCode();
    }

    @Override
    public boolean step() {
        GameObject altar = getAltar();
        return altar != null && altar.isVisible() && altar.interact(getAction());
    }

    @Nonnull
    @Override
    public Pair<WebVertex, WebPath.VertexSearchAction> getStep(Map<String, Object> args) {
        final GameObject altar = getAltar();
        return new Pair<>(
            altar != null && altar.isVisible() ? this : null,
            WebPath.VertexSearchAction.STOP
        );
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "PrayerAltarVertex(name=" + getName() + ", action=" + getAction() + ", x=" +
            position.getX() + ", y=" + position.getY() +
            ", plane=" + position.getPlane() + ')';
    }

    @Override
    public int getOpcode() {
        return 16;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(name.pattern());
        stream.writeInt(name.flags());
        stream.writeUTF(action.pattern());
        stream.writeInt(action.flags());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.name = Pattern.compile(stream.readUTF(), stream.readInt());
        this.action = Pattern.compile(stream.readUTF(), stream.readInt());
        return true;
    }
}
