package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

public enum Rune {
    AIR(1),
    WATER(2),
    EARTH(3),
    FIRE(4),
    MIND(5),
    CHAOS(6),
    DEATH(7),
    BLOOD(8),
    COSMIC(9),
    NATURE(10),
    LAW(11),
    BODY(12),
    SOUL(13),
    ASTRAL(14),
    MIST(15, AIR, WATER),
    MUD(16, EARTH, WATER),
    DUST(17, AIR, EARTH),
    LAVA(18, EARTH, FIRE),
    STEAM(19, WATER, FIRE),
    SMOKE(20, FIRE, AIR),
    WRATH(21);

    private static final Pattern TOME_OF_FIRE = Regex.getPatternForExactString("Tome of fire");
    private static final Pattern TOME_OF_WATER = Regex.getPatternForExactString("Tome of water");
    private static final Pattern KODAI_WAND = Regex.getPatternForExactString("Kodai wand");
    public final int pouchType;
    private final String name;
    private final Pattern staffPattern;
    private final Collection<Rune> replaces;

    Rune(int pouchType, Rune... replaces) {
        this.pouchType = pouchType;
        this.name = name().charAt(0) + name().substring(1).toLowerCase() + " rune";
        this.replaces = Set.of(replaces);
        this.staffPattern = Pattern.compile("(?=.*staff\\b)(?=.*" + name() + "\\b).*", Pattern.CASE_INSENSITIVE);
    }

    public String getName() {
        return name;
    }

    private Predicate<SpriteItem> getSubstitutableEquipmentPredicate() {
        Predicate<SpriteItem> substitutableEquipment = Items.getNamePredicate(staffPattern);
        if (this == FIRE) {
            substitutableEquipment = substitutableEquipment.or(Items.getNamePredicate(TOME_OF_FIRE));
        } else if (this == WATER) {
            substitutableEquipment = substitutableEquipment.or(Items.getNamePredicate(TOME_OF_WATER, KODAI_WAND));
        }
        return substitutableEquipment;
    }

    /**
     * @return true if this Rune replaces other Runes.
     */
    public boolean isCombinationRune() {
        return !replaces.isEmpty();
    }

    /**
     * Returns the quantity of the runes available from the players inventory, RunePouch, and equipment from staves.
     * <p>
     * This is a mildly expensive call.
     *
     * @param provider The List of SpriteItems to get the total quantity of
     * @return the number of runes available for use or Integer.MAX_VALUE if using an item providing unlimited runes.
     */
    public int getQuantity(List<SpriteItem> provider) {
        //All current equipment replacements act as an infinite supply while charged, but we won't concern ourselves with charges in
        //this call.
        if (Items.contains(provider, item -> item.getOrigin() == SpriteItem.Origin.EQUIPMENT)) {
            return Integer.MAX_VALUE;
        }
        int count = Items.getQuantity(provider, name);
        if (Items.containsAnyOf(provider, "Rune pouch", "Divine rune pouch")) {
            count += RunePouch.getQuantity(this);
        }
        if (!isCombinationRune()) {
            for (final Rune rune : values()) {
                if (rune.getReplacedRunes().contains(this)) {
                    count += rune.getQuantity(provider);
                }
            }
        }
        return count;
    }

    public int getQuantity() {
        return getQuantity(getDefaultProvider());
    }

    private List<SpriteItem> getDefaultProvider() {
        final List<String> itemNames = new ArrayList<>();
        Predicate<SpriteItem> predicate = getSubstitutableEquipmentPredicate();
        for (final Rune replacer : getReplacingRunes()) {
            itemNames.add(replacer.getName());
            predicate = predicate.or(replacer.getSubstitutableEquipmentPredicate());
        }
        itemNames.add(name);
        itemNames.add("Rune pouch");
        itemNames.add("Divine rune pouch");
        final List<SpriteItem> items = Inventory.newQuery().names(itemNames.toArray(new String[0])).results().asList();
        items.addAll(Equipment.newQuery().filter(predicate).results().asList());
        return items;
    }

    /**
     * @return the runes that the rune instance replaces, empty if not a combo rune. eg. if this is a MIST rune, return Rune.AIR and Rune.WATER
     */
    public Set<Rune> getReplacedRunes() {
        return replaces.isEmpty() ? EnumSet.noneOf(Rune.class) : EnumSet.copyOf(replaces);
    }

    /**
     * @return the runes that replace the rune instance, empty if a combo rune or has no replacments. eg. if this is a WATER rune, return MIST, MUD and STEAM
     */
    public List<Rune> getReplacingRunes() {
        if (isCombinationRune()) {
            return Collections.emptyList();
        }
        return Arrays.stream(values()).filter(rune -> rune.getReplacedRunes().contains(this)).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "Rune." + name();
    }
}