package com.runemate.game.api.hybrid.location.navigation.web.vertex_types;

import java.util.*;
import java.util.function.*;

public interface FilterableVertex<T> {

    Predicate<T> getFilter();

    void setProvider(List<T> provider);

    void invalidateCache();

}
