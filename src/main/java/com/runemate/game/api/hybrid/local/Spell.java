package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;

public interface Spell {
    String name();

    boolean activate();

    InterfaceComponent getComponent();

    boolean isSelected();

    boolean isAutocasting();

    SpellBook getSpellBook();
}
