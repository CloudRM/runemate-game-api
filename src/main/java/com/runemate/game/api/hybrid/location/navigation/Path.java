package com.runemate.game.api.hybrid.location.navigation;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import javafx.scene.canvas.*;
import javax.annotation.*;

/**
 * A path through the game's landscape that is to be traversed no more than once without regeneration.
 */
public abstract class Path implements Renderable {
    private static final TraversalOption[] EMPTY_TRAVERSAL_OPTIONS = new TraversalOption[0];
    private static final TraversalOption[] DEFAULT_TRAVERSAL_OPTIONS = {
        TraversalOption.MANAGE_RUN,
        TraversalOption.MANAGE_DISTANCE_BETWEEN_STEPS,
        TraversalOption.MANAGE_STAMINA_ENHANCERS
    };

    /**
     * @return Takes a step along the path (manage = true)
     */
    public final boolean step() {
        return step(true);
    }

    /**
     * Takes a step along the path using either all or none of the TraversalOptions
     *
     * @param manage whether or not to manage the distance between steps to prevent spam clicking
     * @return true if it took a step, otherwise false
     */
    public final boolean step(boolean manage) {
        return step(manage ? DEFAULT_TRAVERSAL_OPTIONS : EMPTY_TRAVERSAL_OPTIONS);
    }

    public abstract boolean step(@Nonnull TraversalOption... options);

    /**
     * Gets a list of the vertices in this path
     *
     * @return A List
     */
    public abstract List<? extends Locatable> getVertices();

    /**
     * @return the next Locatable within the path.
     */
    @Nullable
    public abstract Locatable getNext();

    public abstract Locatable getNext(boolean preferViewportTraversal);

    @Override
    public void render(Graphics2D g2d) {
        for (final Locatable l : getVertices()) {
            final Area a = l.getArea();
            if (a != null) {
                a.render(g2d);
            }
        }
    }

    public void render(GraphicsContext gc) {
        for (final Locatable l : getVertices()) {
            if (l instanceof Renderable) {
                ((Renderable) l).render(gc);
            } else {
                final Coordinate c = l.getPosition();
                if (c != null) {
                    final Point p = c.minimap().getInteractionPoint();
                    if (p != null) {
                        gc.fillRect(p.x - 1, p.y - 1, 2, 2);
                    }
                }
            }
        }
    }

    protected boolean isEligibleToStep(@Nonnull TraversalOption... options) {
        if (!Arrays.asList(options).contains(TraversalOption.MANAGE_DISTANCE_BETWEEN_STEPS)) {
            return true;
        }
        Coordinate destination = Traversal.getDestination();
        if (destination == null) {
            return true;
        }
        Player local_player = Players.getLocal();
        if (local_player != null) {
            if (!local_player.isMoving()) {
                return true;
            } else if (Distance.between(local_player.getServerPosition(), destination) <=
                PlayerSense.getAsDouble(PlayerSense.Key.STEP_THRESHOLD)) {
                List<? extends Locatable> vertices = getVertices();
                if (vertices.isEmpty()) {
                    return false;
                }
                return Distance.between(vertices.get(vertices.size() - 1), destination) > 5;
            }
        }
        return false;
    }

    protected boolean triggerRun(@Nonnull TraversalOption... options) {
        if (Arrays.asList(options).contains(TraversalOption.MANAGE_RUN)
            && !Traversal.isRunEnabled()
            &&
            Traversal.getRunEnergy() >= PlayerSense.getAsInteger(PlayerSense.Key.ENABLE_RUN_AT)) {
            return Traversal.toggleRun();
        }
        return true;
    }

    protected boolean triggerStaminaEnhancement(@Nonnull TraversalOption... options) {
        if (Arrays.asList(options).contains(TraversalOption.MANAGE_STAMINA_ENHANCERS)
            && Environment.isOSRS()
            && !Traversal.isStaminaEnhanced()
            && Traversal.getRunEnergy() <=
            PlayerSense.getAsInteger(PlayerSense.Key.ENABLE_STAMINA_ENHANCERS_BELOW)
            && !Inventory.isItemSelected()
            && !Bank.isOpen()) {
            return !Traversal.hasStaminaEnhancer() || Traversal.drinkStaminaEnhancer(false);
        }
        return true;
    }

    public int getLength() {
        int length = 0;
        Locatable previous = null;
        for (Locatable vertex : getVertices()) {
            if (previous != null) {
                var pp = previous.getPosition();
                var cp = vertex.getPosition();

                //If planes are not equal, distance will be Double.POSITIVE_INFINITY
                if (pp != null && cp != null && pp.getPlane() == cp.getPlane()) {
                    length += Distance.between(previous, vertex);
                } else {
                    length++;
                }
            }
            previous = vertex;
        }
        return length;
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder("Path[");
        Iterator<? extends Locatable> iterator = getVertices().iterator();
        while (iterator.hasNext()) {
            string.append(iterator.next());
            if (iterator.hasNext()) {
                string.append(" -> ");
            }
        }
        string.append(']');
        return string.toString();
    }

    /**
     * Options that control the way the path is traversed
     */
    public enum TraversalOption {
        PREFER_VIEWPORT,
        MANAGE_RUN,
        MANAGE_DISTANCE_BETWEEN_STEPS,
        MANAGE_STAMINA_ENHANCERS
    }
}
