package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;

public class GrandExchangeQueryBuilder extends QueryBuilder<GrandExchange.Slot, GrandExchangeQueryBuilder, GrandExchangeQueryResults> {

    private Boolean inUse, buyOffers, sellOffers;
    private Predicate<ItemDefinition> itemDefinitionPredicate;
    private Collection<GrandExchange.Offer.State> states;
    private Collection<GrandExchange.Offer.Type> types;
    private Predicate<Double> completionPredicate;

    @Override
    public boolean accepts(GrandExchange.Slot argument) {
        final GrandExchange.Offer offer = argument.getOffer();
        if (buyOffers != null) {
            boolean condition = false;
            if (offer != null) {
                condition = offer.getType() == GrandExchange.Offer.Type.BUY;
            }
            if (!condition) {
                return false;
            }
        }

        if (sellOffers != null) {
            boolean condition = false;
            if (offer != null) {
                condition = offer.getType() == GrandExchange.Offer.Type.SELL;
            }
            if (!condition) {
                return false;
            }
        }

        if (states != null) {
            boolean condition = false;
            if (offer != null) {
                final GrandExchange.Offer.State offerState = offer.getState();
                for (final GrandExchange.Offer.State state : states) {
                    if (state == offerState) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }

        if (types != null) {
            boolean condition = false;
            if (offer != null) {
                final GrandExchange.Offer.Type offerType = offer.getType();
                for (final GrandExchange.Offer.Type type : types) {
                    if (type == offerType) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }

        if (inUse != null) {
            boolean condition = argument.inUse() == inUse;
            if (!condition) {
                return false;
            }
        }

        if (completionPredicate != null) {
            boolean condition = false;
            if (offer != null) {
                condition = completionPredicate.test(offer.getCompletion());
            }
            if (!condition) {
                return false;
            }
        }

        if (itemDefinitionPredicate != null) {
            boolean condition = false;
            final ItemDefinition definition = offer == null ? null : offer.getItem();
            if (definition != null) {
                condition = itemDefinitionPredicate.test(definition);
            }
            if (!condition) {
                return false;
            }
        }

        return super.accepts(argument);
    }

    public GrandExchangeQueryBuilder completion(double completion) {
        return completion(d -> d == completion);
    }

    public GrandExchangeQueryBuilder completion(Predicate<Double> completionPredicate) {
        this.completionPredicate = completionPredicate;
        return get();
    }

    public GrandExchangeQueryBuilder completed() {
        return completion(100D);
    }

    public GrandExchangeQueryBuilder offerStates(GrandExchange.Offer.State... states) {
        this.states = Arrays.asList(states);
        return get();
    }

    public GrandExchangeQueryBuilder offerTypes(GrandExchange.Offer.Type... types) {
        this.types = Arrays.asList(types);
        return get();
    }

    public GrandExchangeQueryBuilder buyOffers() {
        return buyOffers(true);
    }

    public GrandExchangeQueryBuilder buyOffers(boolean buyOffers) {
        this.buyOffers = buyOffers;
        return get();
    }

    public GrandExchangeQueryBuilder sellOffers() {
        return sellOffers(true);
    }


    public GrandExchangeQueryBuilder sellOffers(boolean sellOffers) {
        this.sellOffers = sellOffers;
        return get();
    }

    public GrandExchangeQueryBuilder inUse(boolean inUse) {
        this.inUse = inUse;
        return get();
    }

    public GrandExchangeQueryBuilder items(Predicate<ItemDefinition> itemDefinitionPredicate) {
        this.itemDefinitionPredicate = itemDefinitionPredicate;
        return get();
    }

    public GrandExchangeQueryBuilder itemNames(String... names) {
        return items(ItemDefinition.getNamePredicate(names));
    }

    @Override
    public GrandExchangeQueryBuilder get() {
        return this;
    }

    @Override
    protected GrandExchangeQueryResults results(
        Collection<? extends GrandExchange.Slot> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new GrandExchangeQueryResults(entries, cache);
    }

    @Override
    public Callable<List<? extends GrandExchange.Slot>> getDefaultProvider() {
        return GrandExchange::getSlots;
    }
}
