package com.runemate.game.api.hybrid.web.vertex.items;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
@ToString
public class BasicItemTeleportVertex extends ItemTeleportVertex {

    public BasicItemTeleportVertex(final Coordinate position, final Pattern action, final SpriteItemQueryBuilder builder) {
        super(position, action, builder);
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        final var item = getItem();
        if (item == null) {
            log.warn("Failed to resolve target entity for {}", this);
            return false;
        }

        return item.interact(action)
            && Execution.delayUntil(() -> !localPos.equals(local.getPosition()), () -> local.getAnimationId() != -1, 3000);
    }
}
