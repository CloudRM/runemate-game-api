package com.runemate.game.api.hybrid.input.direct;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;

@Builder
@ToString
@Log4j2
@Getter
public class MenuAction {

    @Builder.Default
    private final String option = "Automated";
    @Builder.Default
    private final String target = "";
    private final int identifier;
    private final int param0;
    private final int param1;
    private final int opcode;
    @Builder.Default
    private int itemId = -1;
    @Builder.Default
    private Interactable entity = null;

    public static MenuAction forInterfaceComponent(InterfaceComponent component, int action, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(opcode.getId())
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }
    public static MenuAction forInterfaceComponent(InterfaceComponent component, int action) {
        return forInterfaceComponent(component, action, MenuOpcode.getComponentOpcode(component));
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, String action) {
        var opcode = MenuOpcode.getComponentOpcode(component);
        if (opcode == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(opcode.getId())
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forInterfaceComponent(InterfaceComponent component, Pattern action) {
        var opcode = MenuOpcode.getComponentOpcode(component);
        if (opcode == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(MenuOpcode.getComponentMenuIdentifier(component, action))
            .opcode(opcode.getId())
            .param0(component.getParentComponent() == null ? -1 : component.getIndex())
            .param1(component.getId())
            .itemId(-1)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final var index = OpenPlayer.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forPlayer(Player component, int action) {
        return forPlayer(component, MenuOpcode.getPlayerOpcode(action));
    }

    public static MenuAction forPlayer(Player component, String action) {
        return forPlayer(component, MenuOpcode.getPlayerOpcode(action));
    }

    public static MenuAction forPlayer(Player component, Pattern action) {
        return forPlayer(component, MenuOpcode.getPlayerOpcode(action));
    }

    public static MenuAction forNpc(Npc component, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final var index = OpenNpc.indexOf(((Entity) component).uid);
        if (index == -1) {
            return null;
        }
        return MenuAction.builder()
            .identifier(index)
            .opcode(opcode.getId())
            .param0(0)
            .param1(0)
            .entity(component)
            .build();
    }

    public static MenuAction forNpc(Npc component, int action) {
        return forNpc(component, MenuOpcode.getNpcOpcode(action));
    }

    public static MenuAction forNpc(Npc component, String action) {
        return forNpc(component, MenuOpcode.getNpcOpcode(component, action));
    }

    public static MenuAction forNpc(Npc component, Pattern action) {
        return forNpc(component, MenuOpcode.getNpcOpcode(component, action));
    }

    public static MenuAction forGameObject(GameObject component, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final var area = component.getArea();
        if (area == null) {
            return null;
        }
        final var position = area.getBottomLeft();
        final var offset = position.offset();
        return MenuAction.builder()
            .identifier(component.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(component)
            .build();
    }

    public static MenuAction forGameObject(GameObject component, int action) {
        return forGameObject(component, MenuOpcode.getGameObjectOpcode(action));
    }

    public static MenuAction forGameObject(GameObject component, String action) {
        return forGameObject(component, MenuOpcode.getGameObjectOpcode(component, action));
    }

    public static MenuAction forGameObject(GameObject component, Pattern action) {
        return forGameObject(component, MenuOpcode.getGameObjectOpcode(component, action));
    }

    public static MenuAction forGroundItem(GroundItem component, MenuOpcode opcode) {
        if (opcode == null) {
            return null;
        }
        final var position = component.getPosition();
        if (position == null) {
            return null;
        }
        final var offset = position.offset();
        return MenuAction.builder()
            .identifier(component.getId())
            .opcode(opcode.getId())
            .param0(offset.getX())
            .param1(offset.getY())
            .entity(component)
            .build();
    }

    public static MenuAction forGroundItem(GroundItem component, int action) {
        return forGroundItem(component, MenuOpcode.getGroundItemOpcode(action));
    }

    public static MenuAction forGroundItem(GroundItem component, String action) {
        return forGroundItem(component, MenuOpcode.getGroundItemOpcode(component, action));
    }

    public static MenuAction forGroundItem(GroundItem component, Pattern action) {
        return forGroundItem(component, MenuOpcode.getGroundItemOpcode(component, action));
    }

    public static MenuAction forSpriteItem(InterfaceComponent component, SpriteItem item, int action, MenuOpcode opcode) {
        if (opcode == null || component == null) {
            return null;
        }
        return MenuAction.builder()
            .identifier(action == 0 ? 0 : action + 1)
            .opcode(opcode.getId())
            .param0(item.getIndex())
            .param1(component.getId())
            .itemId(item.getId())
            .entity(item)
            .build();
    }

    public static MenuAction forSpriteItem(SpriteItem item, int action, MenuOpcode opcode) {
        final var component = getSpriteItemComponent(item);
        return forSpriteItem(component, item, action, opcode);
    }

    public static MenuAction forSpriteItem(SpriteItem item, int action) {
        final var component = getSpriteItemComponent(item);
        return forSpriteItem(component, item, action, MenuOpcode.getItemOpcode(component, action));
    }

    public static MenuAction forSpriteItem(SpriteItem item, String action) {
        final var component = getSpriteItemComponent(item);
        final var index = MenuOpcode.getItemActionIndex(component, action);
        return forSpriteItem(component, item, index, MenuOpcode.getItemOpcode(component, index));
    }

    public static MenuAction forSpriteItem(SpriteItem item, Pattern action) {
        final var component = getSpriteItemComponent(item);
        final var index = MenuOpcode.getItemActionIndex(component, action);
        return forSpriteItem(component, item, index, MenuOpcode.getItemOpcode(component, index));
    }

    public static InterfaceComponent getSpriteItemComponent(SpriteItem item) {
        //TODO other origin implementations
        switch (item.getOrigin()) {
            case INVENTORY:
                if (Bank.isOpen()) {
                    return Interfaces.getAt(15, 3, item.getIndex());
                }
                return Interfaces.getAt(149, 0, item.getIndex());
            case BANK:
                return Interfaces.getAt(12, 13, item.getIndex());
            case EQUIPMENT:
                return Equipment.Slot.resolve(item.getIndex()).getComponent();
            default:
                return null;
        }
    }

}
