package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.osrs.location.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;

public interface WorldOverview {
    /**
     * Gets the world id (number)
     */
    int getId();

    @OSRSOnly
    EnumSet<WorldType> getWorldTypes();

    @OSRSOnly
    String getActivity();

    boolean isMembersOnly();

    boolean isLootShare();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isDeadman();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isTournament();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isCastleWars();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @RS3Only
    @Deprecated
    boolean isQuickChat();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isPVP();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isBounty();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isPvpArena();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isSpeedrunning();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isHighRisk();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isLastManStanding();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isFreshStart();

    @OSRSOnly
    boolean isSkillTotal500();

    @OSRSOnly
    boolean isSkillTotal750();

    @OSRSOnly
    boolean isSkillTotal1250();

    boolean isSkillTotal1500();

    @OSRSOnly
    boolean isSkillTotal1750();

    boolean isSkillTotal2000();

    @OSRSOnly
    boolean isSkillTotal2200();

    @RS3Only
    @Deprecated
    boolean isSkillTotal2400();

    @RS3Only
    @Deprecated
    boolean isSkillTotal2600();

    @RS3Only
    @Deprecated
    boolean isVIP();

    @RS3Only
    @Deprecated
    boolean isLegacyOnly();

    @RS3Only
    @Deprecated
    boolean isEoCOnly();

    /**
     * @deprecated see {@link #getWorldTypes()}
     */
    @OSRSOnly
    @Deprecated
    boolean isLeague();

    int getPopulation();

    WorldRegion getRegion();
}
