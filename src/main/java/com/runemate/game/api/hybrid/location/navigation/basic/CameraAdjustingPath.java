package com.runemate.game.api.hybrid.location.navigation.basic;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.*;
import javax.annotation.*;

/**
 * TODO deprecate
 */
public class CameraAdjustingPath extends Path {
    private final Path wrapped;

    private CameraAdjustingPath(final Path wrapped) {
        this.wrapped = wrapped;
    }

    /**
     * Converts the provided Path into a CameraAdjustingPath
     *
     * @param path a preferably non-null Path
     * @return A CameraAdjustingPath as long as path is not null
     */
    @Nullable
    public static CameraAdjustingPath convert(final Path path) {
        if (path == null) {
            return null;
        }
        return new CameraAdjustingPath(path);
    }

    @Override
    public List<? extends Locatable> getVertices() {
        return wrapped.getVertices();
    }

    @Override
    public Locatable getNext() {
        return wrapped.getNext();
    }

    @Override
    public Locatable getNext(boolean preferViewportTraversal) {
        return wrapped.getNext(preferViewportTraversal);
    }

    @Override
    public boolean step(@Nonnull TraversalOption... traversalOptions) {
        if (wrapped.step(traversalOptions)) {
            if (!Camera.isTurning()) {
                final Coordinate dest = Traversal.getDestination();
                if (dest != null) {
                    int angle = CommonMath.getAngleOf(dest);
                    if (angle > Random.nextInt(45, 75)) {
                        Camera.concurrentlyTurnTo(dest);
                    }
                }
            }
            return true;
        }
        return false;
    }
}
