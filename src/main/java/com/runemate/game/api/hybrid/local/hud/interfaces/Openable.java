package com.runemate.game.api.hybrid.local.hud.interfaces;

public interface Openable {
    boolean open();

    boolean isOpen();
}
