package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class DepositBoxQueryBuilder extends LocatableEntityQueryBuilder<LocatableEntity, DepositBoxQueryBuilder> {

    @Override
    public DepositBoxQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends LocatableEntity>> getDefaultProvider() {
        return () -> DepositBoxes.getLoaded().asList();
    }
}
