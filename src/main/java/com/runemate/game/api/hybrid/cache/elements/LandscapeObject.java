package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.location.*;
import java.util.*;

public class LandscapeObject {
    private final int id;
    private final int width;
    private final int height;
    private final int regionX;
    private final int regionY;
    private final int mapX;
    private final int mapY;
    private final int shape;
    private final int collisionType;
    private final int orientation;
    private final boolean impassable, impenetrable;
    private final String name;
    private final List<String> actions;
    private final int originalFloor;
    private int floor;
    private Area.Rectangular area;
    private LandscapeObject placeholder;
    private List<LandscapeObject> transformations;

    public LandscapeObject(
        int id, String name, int regionX, int regionY, int mapX, int mapY,
        int floor,
        int width, int height, int shape, int collisionType, int orientation,
        boolean impassable, boolean impenetrable,
        List<String> actions
    ) {
        this.id = id;
        this.regionX = regionX;
        this.regionY = regionY;
        this.mapX = mapX;
        this.mapY = mapY;
        this.width = width;
        this.height = height;
        this.originalFloor = floor;
        this.floor = floor;
        this.shape = shape;
        this.collisionType = collisionType;
        this.orientation = orientation;
        this.name = name;
        this.impassable = impassable;
        this.impenetrable = impenetrable;
        this.actions = actions;
    }

    public int id() {
        return id;
    }

    public String name() {
        return name;
    }

    public LandscapeObject placeholder() {
        return placeholder;
    }

    public void placeholder(LandscapeObject placeholder) {
        this.placeholder = placeholder;
    }

    public boolean transformed() {
        return placeholder != null;
    }

    public List<LandscapeObject> transformations() {
        return transformations;
    }

    public void transformations(List<LandscapeObject> transformations) {
        this.transformations = transformations;
    }

    public boolean transformable() {
        //transformations are null when this object itself is a transformation
        return transformations != null && !transformations.isEmpty();
    }

    public int regionX() {
        return regionX;
    }

    public int regionY() {
        return regionY;
    }

    public int mapX() {
        return mapX;
    }

    public int mapY() {
        return mapY;
    }

    public int originalFloor() {
        return originalFloor;
    }

    public int floor() {
        return floor;
    }

    public void floor(int floor) {
        this.floor = floor;
        this.area = null;
    }

    public Coordinate minimum() {
        return new Coordinate(mapX, mapY, floor);
    }

    public Coordinate maximum() {
        return new Coordinate(mapX + (width - 1), mapY + (height - 1), floor);
    }

    public Coordinate center() {
        return area().getCenter();
    }

    public Area.Rectangular area() {
        if (area != null) {
            return area;
        }
        return area = new Area.Rectangular(minimum(), maximum());
    }

    public int width() {
        return width;
    }

    public int height() {
        return height;
    }

    public int specializedType() {
        return shape;
    }

    public int orientation() {
        return orientation;
    }

    public List<String> actions() {
        return actions != null ? actions : Collections.emptyList();
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + width;
        result = 31 * result + height;
        result = 31 * result + mapX;
        result = 31 * result + mapY;
        result = 31 * result + floor;
        result = 31 * result + shape;
        result = 31 * result + orientation;
        result = 31 * result + name.hashCode();
        if (actions != null) {
            result = 31 * result + actions.hashCode();
        }
        result = 31 * result + Boolean.hashCode(transformed());
        result = 31 * result + Boolean.hashCode(transformable());
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof LandscapeObject) {
            LandscapeObject object = (LandscapeObject) o;
            return id == object.id
                && width == object.width
                && height == object.height
                && mapX == object.mapX
                && mapY == object.mapY
                && floor == object.floor
                && shape == object.shape
                && collisionType == object.collisionType
                && orientation == object.orientation
                && Objects.equals(name, object.name)
                && transformed() == object.transformed()
                && transformable() == object.transformable();
        }
        return false;
    }

    @Override
    public String toString() {
        return "LandscapeObject{" +
            "id=" + id +
            ", name=" + name +
            ", actions=" + actions() +
            ", position=[local(" + regionX +
            ", " + regionY + ")" +
            ", global(" + mapX +
            ", " + mapY +
            ", " + floor +
            ")], width=" + width +
            ", height=" + height +
            ", shape=" + shape +
            ", orientation=" + orientation +
            ", transformed=" + transformed() +
            ", transformable=" + transformable() +
            '}';
    }

    public boolean impassable() {
        return impassable;
    }

    public boolean impenetrable() {
        return impenetrable;
    }

    public int getCollisionType() {
        return collisionType;
    }
}
