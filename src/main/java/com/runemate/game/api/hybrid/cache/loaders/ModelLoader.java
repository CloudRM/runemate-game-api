package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class ModelLoader extends SerializedFileLoader<CacheModel> {

    public ModelLoader() {
        super(CacheIndex.MODELS.getId());
    }

    @Override
    protected CacheModel construct(int entry, int file, Map<String, Object> arguments) {
        return new CacheModel(entry);
    }
}
