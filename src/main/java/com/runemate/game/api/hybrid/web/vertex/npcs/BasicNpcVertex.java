package com.runemate.game.api.hybrid.web.vertex.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import lombok.extern.log4j.*;

@Log4j2
public class BasicNpcVertex extends NpcVertex {

    public BasicNpcVertex(final Coordinate position, final Pattern action, final NpcQueryBuilder builder) {
        super(position, action, builder);
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        final var npc = getNpc();
        if (npc == null) {
            log.warn("Failed to resolve target entity for {}", this);
            return false;
        }

        if (npc.getVisibility() <= 40) {
            Camera.concurrentlyTurnTo(npc);
        }

        return npc.interact(action)
            && Execution.delayUntil(() -> local.isMoving() || local.getAnimationId() != -1 || !localPos.equals(local.getPosition()), 3000)
            && Execution.delayUntil(
            () -> !local.isMoving() && local.getAnimationId() == -1 && !localPos.equals(local.getPosition()),
            () -> local.isMoving() || local.getAnimationId() != -1,
            3000
        );
    }

    @Override
    public String toString() {
        return new StringJoiner(",", "BasicNpcVertex(", ")")
            .add("position=" + position)
            .add("action=" + action.pattern())
            .toString();
    }
}
