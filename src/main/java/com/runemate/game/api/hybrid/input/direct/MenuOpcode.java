package com.runemate.game.api.hybrid.input.direct;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import lombok.*;

@RequiredArgsConstructor
public enum MenuOpcode {
    COMPONENT_TARGET_ON_GAME_OBJECT(2),
    GAME_OBJECT_FIRST_OPTION(3),
    GAME_OBJECT_SECOND_OPTION(4),
    GAME_OBJECT_THIRD_OPTION(5),
    GAME_OBJECT_FOURTH_OPTION(6),
    GAME_OBJECT_FIFTH_OPTION(1001),

    COMPONENT_TARGET_ON_NPC(8),
    NPC_FIRST_OPTION(9),
    NPC_SECOND_OPTION(10),
    NPC_THIRD_OPTION(11),
    NPC_FOURTH_OPTION(12),
    NPC_FIFTH_OPTION(13),

    COMPONENT_TARGET_ON_GROUND_ITEM(17),
    GROUND_ITEM_FIRST_OPTION(18),
    GROUND_ITEM_SECOND_OPTION(19),
    GROUND_ITEM_THIRD_OPTION(20),
    GROUND_ITEM_FOURTH_OPTION(21),
    GROUND_ITEM_FIFTH_OPTION(22),

    WALK(23),

    COMPONENT_TYPE_1(24),
    COMPONENT_TARGET(25),
    COMPONENT_CLOSE(26),
    COMPONENT_TYPE_4(28),
    COMPONENT_TYPE_5(29),
    COMPONENT_CONTINUE(30),

    COMPONENT_FIRST_OPTION(39),
    COMPONENT_SECOND_OPTION(40),
    COMPONENT_THIRD_OPTION(41),
    COMPONENT_FOURTH_OPTION(42),
    COMPONENT_FIFTH_OPTION(43),

    COMPONENT_TARGET_ON_PLAYER(15),
    PLAYER_FIRST_OPTION(44),
    PLAYER_SECOND_OPTION(45),
    PLAYER_THIRD_OPTION(46),
    PLAYER_FOURTH_OPTION(47),
    PLAYER_FIFTH_OPTION(48),
    PLAYER_SIXTH_OPTION(49),
    PLAYER_SEVENTH_OPTION(50),
    PLAYER_EIGHTH_OPTION(51),

    /**
     * normal priority child component actions.
     */
    CC_OP(57),

    COMPONENT_TARGET_ON_COMPONENT(58),

    EXAMINE_OBJECT(1002),
    EXAMINE_NPC(1003),
    EXAMINE_ITEM_GROUND(1004),
    CANCEL(1006),

    CC_OP_LOW_PRIORITY(1007),


    FOLLOW(2046),
    TRADE(2047),

    UNKNOWN(-1);

    private static final Map<Integer, MenuOpcode> byOpcode = new HashMap<>();

    static {
        for (MenuOpcode opcode : values()) {
            byOpcode.put(opcode.getId(), opcode);
        }
    }

    private final int id;

    public int getId() {
        return id;
    }

    public static MenuOpcode of(int id) {
        return byOpcode.getOrDefault(id, UNKNOWN);
    }

    public static MenuOpcode getPlayerOpcode(Predicate<String> filter) {
        return lookup(OpenClient::getPlayerActions, filter, MenuOpcode::getPlayerOpcode);
    }

    public static MenuOpcode getPlayerOpcode(Pattern pattern) {
        return getPlayerOpcode(action -> pattern.matcher(action).matches());
    }

    public static MenuOpcode getPlayerOpcode(String action) {
        return getPlayerOpcode(action::equals);
    }

    public static MenuOpcode getPlayerOpcode(int action) {
        switch (action) {
            case 0:
                return MenuOpcode.PLAYER_FIRST_OPTION;
            case 1:
                return MenuOpcode.PLAYER_SECOND_OPTION;
            case 2:
                return MenuOpcode.PLAYER_THIRD_OPTION;
            case 3:
                return MenuOpcode.PLAYER_FOURTH_OPTION;
            case 4:
                return MenuOpcode.PLAYER_FIFTH_OPTION;
            case 5:
                return MenuOpcode.PLAYER_SIXTH_OPTION;
            case 6:
                return MenuOpcode.PLAYER_SEVENTH_OPTION;
            case 7:
                return MenuOpcode.PLAYER_EIGHTH_OPTION;
            default:
                throw new IllegalArgumentException("action = " + action);
        }
    }

    public static MenuOpcode getNpcOpcode(Npc npc, Predicate<String> filter) {
        final var definition = npc.getActiveDefinition();
        if (definition == null) {
            return null;
        }

        return lookup(definition::getRawActions, filter, MenuOpcode::getNpcOpcode);
    }

    public static MenuOpcode getNpcOpcode(Npc npc, Pattern pattern) {
        return getNpcOpcode(npc, action -> pattern.matcher(action).matches());
    }

    public static MenuOpcode getNpcOpcode(Npc npc, String action) {
        return getNpcOpcode(npc, action::equals);
    }

    public static MenuOpcode getNpcOpcode(int action) {
        switch (action) {
            case 0:
                return MenuOpcode.NPC_FIRST_OPTION;
            case 1:
                return MenuOpcode.NPC_SECOND_OPTION;
            case 2:
                return MenuOpcode.NPC_THIRD_OPTION;
            case 3:
                return MenuOpcode.NPC_FOURTH_OPTION;
            case 4:
                return MenuOpcode.NPC_FIFTH_OPTION;
            default:
                throw new IllegalArgumentException("action = " + action);
        }
    }

    public static MenuOpcode getGroundItemOpcode(GroundItem item, Predicate<String> filter) {
        final var definition = item.getDefinition();
        if (definition == null) {
            return null;
        }

        return lookup(definition::getRawGroundActions, filter, MenuOpcode::getGroundItemOpcode);
    }

    public static MenuOpcode getGroundItemOpcode(GroundItem item, Pattern pattern) {
        return getGroundItemOpcode(item, action -> pattern.matcher(action).matches());
    }

    public static MenuOpcode getGroundItemOpcode(GroundItem item, String action) {
        return getGroundItemOpcode(item, action::equals);
    }

    public static MenuOpcode getGroundItemOpcode(int action) {
        switch (action) {
            case 0:
                return MenuOpcode.GROUND_ITEM_FIRST_OPTION;
            case 1:
                return MenuOpcode.GROUND_ITEM_SECOND_OPTION;
            case 2:
                return MenuOpcode.GROUND_ITEM_THIRD_OPTION;
            case 3:
                return MenuOpcode.GROUND_ITEM_FOURTH_OPTION;
            case 4:
                return MenuOpcode.GROUND_ITEM_FIFTH_OPTION;
            default:
                throw new IllegalArgumentException("action = " + action);
        }
    }

    public static MenuOpcode getGameObjectOpcode(GameObject item, Pattern pattern) {
        return getGameObjectOpcode(item, action -> pattern.matcher(action).matches());
    }

    public static MenuOpcode getGameObjectOpcode(GameObject item, String action) {
        return getGameObjectOpcode(item, action::equals);
    }

    public static MenuOpcode getGameObjectOpcode(GameObject object, Predicate<String> filter) {
        final var definition = object.getDefinition();
        if (definition == null) {
            return null;
        }

        return lookup(definition::getRawActions, filter, MenuOpcode::getGameObjectOpcode);
    }

    public static MenuOpcode getGameObjectOpcode(int action) {
        switch (action) {
            case 0:
                return MenuOpcode.GAME_OBJECT_FIRST_OPTION;
            case 1:
                return MenuOpcode.GAME_OBJECT_SECOND_OPTION;
            case 2:
                return MenuOpcode.GAME_OBJECT_THIRD_OPTION;
            case 3:
                return MenuOpcode.GAME_OBJECT_FOURTH_OPTION;
            case 4:
                return MenuOpcode.GAME_OBJECT_FIFTH_OPTION;
            default:
                throw new IllegalArgumentException("action = " + action);
        }
    }

    public static int getComponentMenuIdentifier(InterfaceComponent item, Pattern pattern) {
        return getComponentMenuIdentifier(item, action -> pattern.matcher(action).matches());
    }

    public static int getComponentMenuIdentifier(InterfaceComponent item, String action) {
        return getComponentMenuIdentifier(item, action::equals);
    }

    public static int getComponentMenuIdentifier(InterfaceComponent component, Predicate<String> filter) {
        final var index = getComponentActionIndex(component, filter);
        return getComponentMenuIdentifier(component, index);
    }

    private static int getComponentActionIndex(InterfaceComponent component, Predicate<String> filter) {
        final var actions = component.getRawActions();
        if (actions == null || actions.length == 0) {
            return -1;
        }
        for (int i = 0; i < actions.length; i++) {
            final var action = actions[i];
            if (action == null || action.isEmpty()) {
                continue;
            }
            if (filter.test(JagTags.remove(action))) {
                return i;
            }
        }
        return -1;
    }

    public static int getComponentMenuIdentifier(InterfaceComponent component, int actionIndex) {
        switch (component.getType()) {
            case SPRITE:
                final var verb = component.getSpellActionName();
                return verb == null || verb.isEmpty() ? actionIndex + 1 : 0;
            case LABEL:
                return 0;
            default:
                return actionIndex + 1;
        }
    }

    public static MenuOpcode getComponentOpcode(InterfaceComponent component) {
        switch (component.getType()) {
            case SPRITE: {
                final var verb = component.getSpellActionName();
                return verb == null || verb.isEmpty() ? CC_OP : COMPONENT_TARGET;
            }
            case LABEL: {
                return COMPONENT_CONTINUE;
            }
            default:
                return CC_OP;
        }
    }

    public static int getItemActionIndex(InterfaceComponent item, Pattern pattern) {
        return getItemActionIndex(item, action -> pattern.matcher(action).matches());
    }

    public static int getItemActionIndex(InterfaceComponent item, String action) {
        return getItemActionIndex(item, action::equals);
    }

    public static int getItemActionIndex(InterfaceComponent object, Predicate<String> filter) {
        final var actions = object.getRawActions();
        if (actions == null || actions.length == 0) {
            return -1;
        }
        for (int i = 0; i < actions.length; i++) {
            var action = actions[i];
            if (action == null || action.isEmpty()) {
                continue;
            }
            if (filter.test(JagTags.remove(action))) {
                return i;
            }
        }
        return -1;
    }

    public static MenuOpcode getItemOpcode(InterfaceComponent component, int action) {
        if (action == 0) {
            final var actions = component.getRawActions();
            if (actions == null || actions[0] == null) {
                return MenuOpcode.COMPONENT_TARGET;
            }
        }
        return action >= 4 ? MenuOpcode.CC_OP_LOW_PRIORITY : MenuOpcode.CC_OP;
    }

    private static MenuOpcode lookup(Supplier<String[]> supplier, Predicate<String> filter, Function<Integer, MenuOpcode> func) {
        final var actions = supplier.get();
        if (actions == null || actions.length == 0) {
            return null;
        }
        for (int i = 0; i < actions.length; i++) {
            var action = actions[i];
            if (action == null || action.isEmpty()) {
                continue;
            }
            if (filter.test(JagTags.remove(action))) {
                return func.apply(i);
            }
        }
        return null;
    }
}