package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.script.annotations.*;

@RS3Only
@Deprecated
public final class MoneyPouch {
    private MoneyPouch() {
    }

    /**
     * Gets the amount of money in the money pouch
     */
    public static int getContents() {
        return 0;
    }
}
