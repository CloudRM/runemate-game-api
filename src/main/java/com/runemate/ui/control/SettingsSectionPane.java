package com.runemate.ui.control;

import com.google.common.base.*;
import com.google.common.collect.*;
import com.runemate.ui.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.stream.*;
import javafx.geometry.*;
import javafx.scene.control.*;

public class SettingsSectionPane extends TitledPane {

    private final SettingsSectionDescriptor section;
    private final SettingsGridPane contents;
    private final DefaultUI parent;

    public SettingsSectionPane(DefaultUI parent, SettingsSectionDescriptor section, boolean collapsible) {
        this.parent = parent;
        this.section = section;
        this.contents = new SettingsGridPane();
        parent.bot().getEventDispatcher().addListener(contents);

        setMaxHeight(Integer.MAX_VALUE);

        setCollapsible(collapsible);
        setContent(contents);
        setPadding(new Insets(4));

        if (section != null) {
            setText(section.title());
            setExpanded(!section.section().collapsed());
            setTooltip(new Tooltip(section.section().description()));
        } else {
            setText("General");
            setExpanded(true);
        }

        getStyleClass().add("settings-section");
    }

    public SettingsSectionPane(DefaultUI parent, SettingsSectionDescriptor section) {
        this(parent, section, false);
    }

    public SettingsSectionPane(DefaultUI parent) {
        this(parent, null, false);
    }

    public void init(SettingsManager manager, SettingsDescriptor settings) {
        final var items = settings.settings().stream()
            .filter(this::belongsToSection)
            .filter(sd -> !sd.hidden())
            .sorted((a, b) -> ComparisonChain.start()
                .compare(a.order(), b.order())
                .compare(a.title(), b.title())
                .result())
            .collect(Collectors.toList());

        if (items.isEmpty()) {
            setVisible(false);
            setManaged(false);
        }

        contents.addItems(parent, manager, settings, items);
    }

    private boolean belongsToSection(SettingDescriptor setting) {
        if (section == null) {
            return Strings.isNullOrEmpty(setting.setting().section());
        }
        return section.key().equals(setting.setting().section());
    }
}
