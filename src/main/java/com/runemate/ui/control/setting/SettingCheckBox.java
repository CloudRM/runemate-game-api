package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.*;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class SettingCheckBox extends CheckBox implements SettingControl {

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;

    public SettingCheckBox(final SettingsManager manager, final SettingsDescriptor group, final SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());

        selectedProperty().addListener((obs, old, value) -> manager.set(group, setting, String.valueOf(value)));

        setSelected(Boolean.parseBoolean(manager.get(group, setting)));

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }



}
