package com.runemate.ui.control.setting;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.control.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import java.util.*;
import javafx.beans.property.*;
import javafx.geometry.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@Getter
@Accessors(fluent = true)
public class SettingEquipmentButton extends Button implements SettingControl {

    private final ObjectProperty<EquipmentLoadout> valueProperty;

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;
    private final SettingConverter converter;

    public SettingEquipmentButton(BotPlatform bot, SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;
        this.converter = new EquipmentLoadout.SettingConverter();

        setText("Equipment");

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());

        valueProperty = new SimpleObjectProperty<>();

        final var initial = manager.get(group(), setting());
        if (initial != null && !initial.isEmpty()) {
            EquipmentLoadout loadout;
            try {
                loadout = (EquipmentLoadout) converter.fromString(initial, EquipmentLoadout.class);
            } catch (Exception e) {
                log.warn("Failed to unmarshal previous equipment loadout", e);
                loadout = new EquipmentLoadout();
            }
            valueProperty.set(loadout);
            setText(loadout.isEmpty() ? "Not set" : loadout.size() + " items");
        }

        valueProperty.addListener((obs, old, value) -> {
            if (value.size() == 0) {
                manager.remove(group(), setting());
                setText("Not set");
            } else {
                final var text = converter.toString(value);
                manager.set(group(), setting(), text);
                setText(value.size() + " items");
            }
        });

        setOnAction(event -> {
            setText("Loading...");
            final var previous = valueProperty.get();
            final var dialog = previous == null ? new SettingEquipmentDialog(bot) : new SettingEquipmentDialog(bot, previous);
            final var value = dialog.showAndWait();
            value.ifPresentOrElse(valueProperty::set, () -> valueProperty.set(new EquipmentLoadout()));
        });

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }

    private static class SettingEquipmentDialog extends Dialog<EquipmentLoadout> {

        public SettingEquipmentDialog(final BotPlatform bot) {
            this(bot, new EquipmentLoadout());
        }

        public SettingEquipmentDialog(final BotPlatform bot, @NonNull EquipmentLoadout existing) {
            setTitle("Equipment selector");

            final Map<Equipment.Slot, SlotControl> slots = new EnumMap<>(Equipment.Slot.class);
            List<ItemDefinition> candidates = null;
            try {
                candidates = bot.invokeAndWait(() -> ItemDefinition.loadAll(existing.loadout()));
            } catch (Exception e) {
                log.warn("Failed to look up existing ItemDefinitions", e);
            }
            for (var slot : Equipment.Slot.values()) {
                var initial = candidates != null ? existing.getDefinition(slot, candidates) : null;
                if (initial != null) {
                    slots.put(slot, new SlotControl(bot, slot, initial));
                } else {
                    slots.put(slot, new SlotControl(bot, slot));
                }
            }

            var grid = new GridPane();
            var hConstraints = new ColumnConstraints(36, 36, 36);
            var vConstraints = new RowConstraints(36, 36, 36);
            grid.setVgap(1);
            grid.setHgap(1);
            grid.getColumnConstraints().add(hConstraints);
            grid.getRowConstraints().add(vConstraints);
            grid.setPadding(new Insets(4));
            grid.add(slots.get(Equipment.Slot.HEAD), 1, 0);
            grid.add(slots.get(Equipment.Slot.CAPE), 0, 1);
            grid.add(slots.get(Equipment.Slot.NECK), 1, 1);
            grid.add(slots.get(Equipment.Slot.AMMUNITION), 2, 1);
            grid.add(slots.get(Equipment.Slot.WEAPON), 0, 2);
            grid.add(slots.get(Equipment.Slot.BODY), 1, 2);
            grid.add(slots.get(Equipment.Slot.SHIELD), 2, 2);
            grid.add(slots.get(Equipment.Slot.LEGS), 1, 3);
            grid.add(slots.get(Equipment.Slot.HANDS), 0, 4);
            grid.add(slots.get(Equipment.Slot.FEET), 1, 4);
            grid.add(slots.get(Equipment.Slot.RING), 2, 4);

            var content = new VBox(4.0);
            content.getChildren().add(grid);

            final var refresh = new Button("Refresh");
            final var clear = new Button("Clear");

            refresh.setPrefWidth(110);
            clear.setPrefWidth(110);

            refresh.setOnAction(e -> slots.values().forEach(SlotControl::refresh));
            clear.setOnAction(e -> slots.values().forEach(SlotControl::clear));

            content.getChildren().addAll(refresh, clear);

            final var ok = new ButtonType("Save", ButtonBar.ButtonData.OK_DONE);
            getDialogPane().getButtonTypes().add(ok);
            getDialogPane().setContent(content);
            setResultConverter(button -> {
                if (ok.equals(button)) {
                    return null;
                }

                var results = new EquipmentLoadout();
                for (var slot : slots.entrySet()) {
                    results.put(slot.getKey(), slot.getValue().getPattern());
                }
                return results;
            });
        }
    }
}
