package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import java.util.Objects;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class SettingDoubleSpinner extends Spinner<Double> implements SettingControl {

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;

    public SettingDoubleSpinner(final SettingsManager manager, final SettingsDescriptor group, final SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());
        setEditable(true);

        valueProperty().addListener((obs, old, value) -> manager.set(group, setting, String.valueOf(value)));

        final var initialStr = manager.get(group, setting);
        final var initial = Strings.isNullOrEmpty(initialStr) ? 0 : Double.parseDouble(initialStr);
        setValueFactory(new SpinnerValueFactory.DoubleSpinnerValueFactory(0, Double.MAX_VALUE, initial, 0.1));

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }

}
