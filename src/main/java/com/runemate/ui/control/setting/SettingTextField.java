package com.runemate.ui.control.setting;

import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import java.util.Objects;
import java.util.stream.*;
import javafx.scene.control.*;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class SettingTextField extends TextField implements SettingControl {

    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;

    public SettingTextField(final SettingsManager manager, final SettingsDescriptor group, final SettingDescriptor setting) {
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());

        textProperty().addListener((obs, old, value) -> manager.set(group, setting, String.valueOf(value)));

        setText(manager.get(group, setting));
        //We use setOnAction rather than listening to the textProperty() because we don't want I/O every time the user makes a keystroke
        setOnAction(e -> {
            if (Strings.isNullOrEmpty(getText())) {
                manager.remove(group, setting);
            } else {
                manager.set(group, setting, getText());
            }
        });

        //Also send a change when the field loses focus
        focusedProperty().addListener((obs, oldValue, newValue) -> {
            if (oldValue && !newValue) {
                if (Strings.isNullOrEmpty(getText())) {
                    manager.remove(group, setting);
                } else {
                    manager.set(group, setting, getText());
                }
            }
        });

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }


}
