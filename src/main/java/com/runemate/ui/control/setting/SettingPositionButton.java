package com.runemate.ui.control.setting;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.ui.control.*;
import com.runemate.ui.setting.descriptor.open.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import java.util.Objects;
import lombok.*;
import lombok.experimental.*;

@Getter
@Accessors(fluent = true)
public class SettingPositionButton extends ValueButton<Coordinate> implements SettingControl {
    private final SettingsManager manager;
    private final SettingsDescriptor group;
    private final SettingDescriptor setting;


    public SettingPositionButton(BotPlatform bot, SettingsManager manager, SettingsDescriptor group, SettingDescriptor setting) {
        super(() -> sneakyCoordinate(bot), new CoordinateSettingConverter());
        this.manager = manager;
        this.group = group;
        this.setting = setting;

        managedProperty().bind(visibleProperty());
        setDisable(setting.setting().disabled());
        setFocusTraversable(true);
        disableProperty().bind(manager.lockedProperty());

        valueProperty().addListener((obs, old, value) -> {
            var text = getValueString();
            if (Strings.isNullOrEmpty(text)) {
                manager.remove(group, setting);
            } else {
                manager.set(group, setting, text);
            }
        });

        if (!isDependencyMet()) {
            setVisible(false);
        }
    }

    @SneakyThrows
    private static Coordinate sneakyCoordinate(BotPlatform platform) {
        return platform.invokeAndWait(() -> {
            final var local = Players.getLocal();
            return local != null ? local.getPosition() : null;
        });
    }
}
