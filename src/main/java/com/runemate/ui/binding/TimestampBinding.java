package com.runemate.ui.binding;

import com.runemate.game.api.hybrid.util.*;
import javafx.beans.binding.*;
import javafx.collections.*;

public class TimestampBinding extends StringBinding {

    private final NumberExpression expression;

    public TimestampBinding(final NumberExpression expression) {
        this.expression = expression;
        super.bind(expression);
    }

    @Override
    protected String computeValue() {
        return Time.format(Math.max(0, expression.longValue()));
    }

    @Override
    public void dispose() {
        super.unbind(expression);
    }

    @Override
    public ObservableList<?> getDependencies() {
        return FXCollections.singletonObservableList(expression);
    }
}
